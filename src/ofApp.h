#pragma once

#include "ofMain.h"
#include "ofxKinect.h"
#include "ofxCvHaarFinder.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		ofxKinect kinect;
		ofxCvGrayscaleImage depthImage;
		ofxCvGrayscaleImage diffImage;
		bool diffImageRequested;
		vector <ofSoundPlayer> farSounds;
		vector <ofSoundPlayer> closeSounds;
		float tiltAngle;
		unsigned int treshold;
		bool someoneNear;
		ofSoundPlayer * playing;
		bool intermission;
		long intermissionStarted;
		long intermissionLength;
		int lastSoundIndex;

};
