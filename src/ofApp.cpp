#include "ofApp.h"
#include "ofxKinect.h"

//--------------------------------------------------------------
void ofApp::setup(){

    kinect.setRegistration(true);
    //kinect.setDepthClipping(500,3000);
    kinect.init();
    kinect.open();
    depthImage.allocate(640,480);
    diffImage.allocate(640,480);
    treshold = 1800000;
    someoneNear = false;

    if(kinect.isConnected())
    {
        ofLogNotice() << "KINECT connected!";
        tiltAngle = kinect.getCurrentCameraTiltAngle();
        cout << tiltAngle << endl;
        diffImageRequested = true;
    }
    else
    {
        //exit();
        ofLogWarning() << "KINECT NOT connected!";
    }

    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("come_here_1.wav");
    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("come_here_2.wav");
    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("if_you_want_a_boxer_1.wav");
    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("if_you_want_a_boxer_2.wav");
    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("im_alone_1.wav");
    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("im_alone_2.wav");
    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("im_aloner_1.wav");
    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("im_aloner_2.wav");
    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("is_there_anybody_in_here_1.wav");
    farSounds.push_back(ofSoundPlayer());
    farSounds.back().loadSound("is_there_anybody_in_there_1.wav");
   // sounds.back().play();
    closeSounds.push_back(ofSoundPlayer());
    closeSounds.back().loadSound("go_away_1.wav");
    closeSounds.push_back(ofSoundPlayer());
    closeSounds.back().loadSound("please_go_away_1.wav");
    closeSounds.push_back(ofSoundPlayer());
    closeSounds.back().loadSound("please_go_away_2.wav");


    playing = &farSounds.back();

    kinect.setDepthClipping(500,5000);

    cout << kinect.getFarClipping() << endl;

    intermission = true;
    intermissionStarted = ofGetElapsedTimeMillis();

}

//--------------------------------------------------------------
void ofApp::update() {

    if (!playing->getIsPlaying()) {
        if (!intermission) {
            intermission = true;
            intermissionStarted = ofGetElapsedTimeMillis();
            intermissionLength = 3000 + rand() % 7000;
        } if (ofGetElapsedTimeMillis() - intermissionStarted > intermissionLength ) {
            intermission = false;
            int i = lastSoundIndex;
            while (i==lastSoundIndex) {
                if (someoneNear) {
                    i = rand() % closeSounds.size();
                } else {
                    i = rand() % farSounds.size();
                }
            }
            lastSoundIndex = i;
            if (someoneNear) {
                playing = &closeSounds[lastSoundIndex];
            } else {
                playing = &farSounds[lastSoundIndex];
            }
            playing->play();
        }
    }



/*        kinect.update();
        if (kinect.isFrameNew()) {
            depthImage.setFromPixels(kinect.getDepthPixels(), kinect.width, kinect.height);
            if (diffImageRequested) {
                diffImage = depthImage;
                diffImageRequested = false;
            }
            depthImage.absDiff(diffImage, depthImage);
            depthImage.threshold(20);
            unsigned int diff = 0;
            unsigned char * pix = depthImage.getPixels();
            for (int i = 0; i < kinect.width * kinect.height; i++) {
                    diff += pix[i];
            }
            if (diff > treshold) {
                kinect.setLed((ofxKinect::LedMode)3);
                someoneNear = true;
            } else {
                kinect.setLed((ofxKinect::LedMode)2);
                someoneNear = false;
            }
            cout << diff << endl;
        }

        if (!playing->getIsPlaying()) {
            if (someoneNear) {
                playing = &closeSounds[rand() % closeSounds.size()];
                playing->play();
            } else {
                playing = &farSounds[rand() % farSounds.size()];
                playing->play();
            }
        }
*/
}

//--------------------------------------------------------------
void ofApp::draw(){
    depthImage.draw(0,0);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key==OF_KEY_UP) {
        if (tiltAngle < 30) {
            tiltAngle++;
            kinect.setCameraTiltAngle(tiltAngle);
        }
    } else if (key==OF_KEY_DOWN) {
         if (tiltAngle > -30) {
            tiltAngle--;
            kinect.setCameraTiltAngle(tiltAngle);
        }
    } else if (key==' ') {
        diffImageRequested = true;
    } else if (key=='f') {
        someoneNear = false;
    } else if (key=='c') {
        someoneNear = true;
        intermission = true;
        intermissionStarted = 0;
    } else if (key=='f') {
        kinect.setLed((ofxKinect::LedMode)3);
    } else if (key=='r') {
        kinect.setLed((ofxKinect::LedMode)0);
    }

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
