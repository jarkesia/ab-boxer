#include <signal.h>
#include <vector>
#include <string>
#include <wiringPi.h>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <iostream>

using namespace std;

bool run = true;

vector<string> farSounds;
vector<string> closeSounds;
vector<string> aloneAgainSounds;
vector<string> poem;
float tiltAngle;
unsigned int treshold;
bool someoneNear;
bool someoneJustLeft;
string * playing;
int lastSoundIndex;
int poemIndex = 0;

void exitClient(int sig) {
	run = false;
}


int main(int argc, char** argv) {

	signal(SIGINT, exitClient);
	signal(SIGTERM, exitClient);

	farSounds.push_back("come_here_1.wav");
	farSounds.push_back("come_here_2.wav");
	farSounds.push_back("if_you_want_a_boxer_1.wav");
	farSounds.push_back("if_you_want_a_boxer_2.wav");
	farSounds.push_back("im_alone_1.wav");
	farSounds.push_back("im_alone_2.wav");
	farSounds.push_back("let_it_be_2.wav");
	farSounds.push_back("im_aloner_1.wav");
	farSounds.push_back("im_aloner_2.wav");
	farSounds.push_back("is_there_anybody_in_here_1.wav");
	farSounds.push_back("is_there_anybody_in_there_1.wav");

	poem.push_back("as_butterfly.wav");
	poem.push_back("remember_me.wav");
	poem.push_back("do_not_surrender.wav");
	poem.push_back("kiss_is_just_a_rose.wav");
	poem.push_back("i_suppose.wav");

	aloneAgainSounds.push_back("dont_leave_me_alone_1.wav");
	aloneAgainSounds.push_back("come_back_2.wav");
	aloneAgainSounds.push_back("please_come_back_1.wav");
	aloneAgainSounds.push_back("come_here_1.wav");

	closeSounds.push_back("go_away_1.wav");
	closeSounds.push_back("please_go_away_1.wav");
	closeSounds.push_back("please_go_away_2.wav");
	closeSounds.push_back("leave_me_alone_please_1.wav");
	closeSounds.push_back("let_me_be_2.wav");
	closeSounds.push_back("leave_me_alone_1.wav");

	wiringPiSetup();
	pinMode(2,INPUT);

	while (run) {

		int i = lastSoundIndex;
		while (i==lastSoundIndex) {
			if (someoneNear) {
				i = rand() % closeSounds.size();
			} else if (someoneJustLeft) {
				i = rand() % aloneAgainSounds.size();
			} else {
				i = rand() % (farSounds.size()+1);
				if (i == (int)farSounds.size()) {
					/* read a poem */
					poemIndex = 0;
				}
			}
		}

		lastSoundIndex = i;
		string s("aplay /usr/share/annabreu/ettfel/");
		if (someoneNear) {
			s = s + closeSounds[lastSoundIndex].c_str();
			system(s.c_str());
		} else if (someoneJustLeft) {
			s = s + aloneAgainSounds[lastSoundIndex].c_str();
			system(s.c_str());
			if ((rand() % 3) == 0) {
				someoneJustLeft = false;
			}
		} else {
			/* rock is alone */
			if (poemIndex > -1) {
				s = s + poem[poemIndex].c_str();
				poemIndex++;
				if (poemIndex == (int)poem.size()) poemIndex = -1;
			} else {
				s = s + farSounds[lastSoundIndex].c_str();
			}
			system(s.c_str());
		}

		int t = millis();
		unsigned int intermissionLength = 4000 + rand() % 10000;
		while (millis() - t < intermissionLength ) {
			delay(100);
			bool b = digitalRead(2);
			if (b != someoneNear) {
				someoneNear = b;
				if (someoneNear ) {
					poemIndex = -1;
					break;
				} else {
					someoneJustLeft = true;
					intermissionLength += 8000;
				}
			}
			if (poemIndex > -1) break;

			if (!run) break;
		}
	}

	return 0;
}


